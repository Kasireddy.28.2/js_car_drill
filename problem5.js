//let dataOfCars=require("./data");

function noOfCarsBefore2000Year(array,year){
    let count=0;

    for (let data of array){
        if(data.car_year>year){
            count+=1;
        }
    };

    return count;
};


module.exports=noOfCarsBefore2000Year;

//console.log(noOfCarsBefore2000Year(dataOfCars,2000));