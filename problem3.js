//let dataOfCars=require("./data");

function carModelsIntoAlphabeticalOrder(array){
    let car_models_alphabetical=[];
    for(let data of array){
        car_models_alphabetical.push(data.car_model);
    }

    return car_models_alphabetical.sort();
}

module.exports=carModelsIntoAlphabeticalOrder;


//console.log(carModelsIntoAlphabeticalOrder(dataOfCars));