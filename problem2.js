//let dataOfCars=require("./data");

function lastCarInData(array){
    let id=array.length;
    for(let data of array){
        if(data.id===id){
            return `Last car is a car ${data.car_make} ${data.car_model}`;
        }
    }
    
};

module.exports=lastCarInData;