//let dataOfCars=require("./data");

function InformationOfCarWithCarsId(array,id){
    for(let data of array){
        if(data.id===id){
            return `Car ${id} is a ${data.car_year} ${data.car_make} ${data.car_model}`
        }
    }
};

module.exports=InformationOfCarWithCarsId;